package me.FinestDev.Deathmatch.Arenas;

import me.FinestDev.Deathmatch.Main;

import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

public class Arena{
	
	Main pl;
	public Arena(Main in){
		pl = in;
	}

	//you want some info about the arena stored here
	int id;//the arena id
	Location spawn = null;//spawn location for the arena
	List<String> players;//list of players
	List<String> redTeam;
	List<String> blueTeam;
	Integer redKills;
	Integer blueKills;
	Location redSpawn;
	Location blueSpawn;


	//now let's make a few getters/setters, and a constructor
	public Arena(Location loc, Location red, Location blue, int id){
		this.spawn = loc;
		this.id = id;
		this.redSpawn = red;
		this.blueSpawn = blue;
		this.players = new ArrayList<String>();
		this.redTeam = new ArrayList<String>();
		this.blueTeam = new ArrayList<String>();
		this.redKills = 0;
		this.blueKills = 0;
	}

	public int getId(){
		return this.id;
	}

	public List<String> getPlayers(){
		return this.players;
	}
	
}