package me.FinestDev.Deathmatch.Arenas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.FinestDev.Deathmatch.Main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ArenaManager {

	Main pl;
	public ArenaManager(Main in){
		pl = in;
	}

	//the class instance
	private static ArenaManager am;
	//a few other fields
	public Map<String, ItemStack[]> inv = new HashMap<String, ItemStack[]>();
	public Map<String, ItemStack[]> armor = new HashMap<String, ItemStack[]>();
	//list of arenas
	public static List<Arena> arenas = new ArrayList<Arena>();
	int arenaSize = 0;

	public ArenaManager(){
	}

	//we want to get an instance of the manager to work with it statically
	public static ArenaManager getManager(){
		if(am == null)
			am = new ArenaManager();

		return am; // NOT THREAD SAFE!
	}

	public void ArenaMessage(Arena a, String msg){
		for(String s : a.getPlayers()){
			Bukkit.getPlayer(s).sendMessage(ChatColor.translateAlternateColorCodes('$', msg));
		}
	}

	//get an Arena object from the list
	public Arena getArena(int i){
		for(Arena a : arenas){
			if(a.getId() == i){
				return a;
			}
		}
		return null;
	}

	public Arena getPlayer(Player p){
		for(Arena a : arenas){
			if(a.players.contains(p.getName())){
				return a;
			}
		}
		return null;
	}

	public void clearTeams(Arena a){
		a.redTeam.clear();
		a.blueTeam.clear();
	}

	public List<String> getRedTeam(Arena a){
		return a.redTeam;
	}

	public List<String> getBlueTeam(Arena a){
		return a.blueTeam;
	}

	public Integer getRedKills(Arena a){
		return a.redKills;
	}

	public Integer getBlueKills(Arena a){
		return a.blueKills;
	}

	public void setRedKills(Arena a){
		a.redKills++;
	}

	public void setBlueKills(Arena a){
		a.blueKills++;
	}

	//add players to the arena, save their inventory
	public void addPlayer(Player p, int i){
		Arena a = getArena(i);//get the arena you want to join
		if(a == null){//make sure it is not null
			p.sendMessage(ChatColor.RED+"Invalid arena!");
			return;
		}

		if(isInGame(p)){
			p.sendMessage(ChatColor.RED+"You're already in a game!");
			return;
		}

		if(a.getPlayers().size() >= pl.getConfig().getInt("Game.MaxPlayers")){
			p.sendMessage("�cThere are too many players in this match!");
			return;
		}

		if(GameStates.arenajoin.get(a.id) == true){

			a.getPlayers().add(p.getName());//add them to the arena list of players
			inv.put(p.getName(), p.getInventory().getContents());//save inventory
			armor.put(p.getName(), p.getInventory().getArmorContents());

			p.getInventory().setArmorContents(null);
			p.getInventory().clear();

			p.teleport(a.spawn);//teleport to the arena spawn
			p.sendMessage("�6You have joined arena �b"+i+"�6!");

		}else{
			p.sendMessage("�6This arena is already in progress!");
		}

	}

	//remove players
	public void removePlayer(Player p){
		Arena a = null;//make an arena
		for(Arena arena : arenas){
			if(arena.getPlayers().contains(p.getName())){
				a = arena;//if the arena has the player, the arena field would be the arena containing the player
			}
			//if none is found, the arena will be null
		}
		if(a == null || !a.getPlayers().contains(p.getName())){//make sure it is not null
			p.sendMessage(ChatColor.RED+"Invalid operation!");
			return;
		}

		a.getPlayers().remove(p.getName());//remove from arena

		if(a.redTeam.contains(p.getName())){ a.redTeam.remove(p.getName()); }
		if(a.blueTeam.contains(p.getName())){ a.blueTeam.remove(p.getName()); }

		p.getInventory().clear();
		p.getInventory().setArmorContents(null);

		p.getInventory().setContents(inv.get(p.getName()));//restore inventory
		p.getInventory().setArmorContents(armor.get(p.getName()));

		inv.remove(p.getName());//remove entries from hashmaps
		armor.remove(p.getName());
		p.teleport( pl.util.fromString(pl.getConfig().getString("Arenas."+a.getId()+".Lobby")) );

		p.setFireTicks(0);

		p.sendMessage("�cYou have left the arena!");

		pl.score.removeScoreBoard(p);

	}

	//create arena
	public Arena createArena(Location l, Location red, Location blue){

		int num = arenaSize + 1;
		arenaSize++;

		Arena a = new Arena(l, red, blue, num);
		arenas.add(a);

		pl.getConfig().set("Arenas." + num+".Red", serializeLoc(red));
		pl.getConfig().set("Arenas." + num+".Blue", serializeLoc(blue));
		pl.getConfig().set("Arenas." + num+".Lobby", serializeLoc(l));

		List<Integer> list = pl.getConfig().getIntegerList("Arenas.List");

		list.add(num);

		pl.getConfig().set("Arenas.List", list);

		pl.saveConfig();

		pl.timers.startLobbyTimer(a);

		return a;

	}

	public boolean isInGame(Player p){
		for(Arena a : arenas){
			if(a.getPlayers().contains(p.getName()))
				return true;
		}
		return false;
	}

	public Arena reloadArena(Location l, Location red, Location blue) {
		int num = arenaSize + 1;
		arenaSize++;

		Arena a = new Arena(l, red, blue, num);
		arenas.add(a);

		pl.score.createBoard(a);

		return a;
	}

	public void resetArena(Arena a){
		
		for(Player p : Bukkit.getOnlinePlayers()){
			if(this.isInGame(p)){
				if(this.getPlayer(p).equals(a)){
					this.removePlayer(p);
				}
			}
		}
		
		a.redKills = 0;
		a.blueKills = 0;
		a.redTeam.clear();
		a.blueTeam.clear();
		GameStates.arenajoin.remove(a.getId());
		GameStates.arenatime.remove(a.getId());

		pl.timers.startLobbyTimer(a);

	}
	

	public void loadGames(){
		arenaSize = 0;

		if(pl.getConfig().getIntegerList("Arenas.List").isEmpty()){
			return;
		}

		for(int i : pl.getConfig().getIntegerList("Arenas.List")){
			Arena a = reloadArena(deserializeLoc(pl.getConfig().getString("Arenas." +i+".Lobby")), deserializeLoc(pl.getConfig().getString
					("Arenas."+i+".Red")), deserializeLoc(pl.getConfig().getString("Arenas."+i+".Blue")));
			a.id = i;


			pl.timers.startLobbyTimer(a);

		}

	}

	public void checkGame(Arena a){
		if(a.redTeam.size() == 0){
			pl.end.endGame(a, "Blue");
		}else
			if(a.blueTeam.size() == 0){
				pl.end.endGame(a, "Red");
			}
	}

	public String serializeLoc(Location l){
		return l.getWorld().getName()+","+l.getBlockX()+","+l.getBlockY()+","+l.getBlockZ();
	}

	public Location deserializeLoc(String s){
		String[] st = s.split(",");
		return new Location(Bukkit.getWorld(st[0]), Integer.parseInt(st[1]), Integer.parseInt(st[2]), Integer.parseInt(st[3]));
	}
}
