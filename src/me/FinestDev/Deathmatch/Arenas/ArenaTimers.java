package me.FinestDev.Deathmatch.Arenas;

import java.util.HashMap;

import me.FinestDev.Deathmatch.Main;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;

public class ArenaTimers {

	Main pl;
	public ArenaTimers(Main in){
		pl = in;
	}
	
	private static HashMap<Arena, Integer> ids = new HashMap<Arena, Integer>();

	public void startLobbyTimer(final Arena arena){
		GameStates.arenatime.put(arena.getId(), pl.getConfig().getInt("Lobby.Timer"));
		GameStates.arenajoin.put(arena.getId(), true);

		int id = Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, new Runnable() {
			public void run(){

				int timer = GameStates.arenatime.get(arena.getId());

				if(timer >= 0){
					GameStates.arenatime.remove(arena.getId());
					GameStates.arenatime.put(arena.getId(), timer-1);
					if(timer % 60 == 0 && timer != 0){

						broadcastTime("Lobby", arena, timer);

					}else
						if(timer < 5 && timer != 0 || timer == 30){
							broadcastTime("Lobby", arena, timer);
						}else
							if(timer == 0){
								// Begin the game
								if(arena.getPlayers().size() >= pl.getConfig().getInt("Game.RequiredPlayers")){
									cancel(arena);
									startGameTimer(arena);
									pl.start.startGame(arena);
								}else{
									pl.am.ArenaMessage(arena, ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Lobby.NotEnough")) );
									GameStates.arenatime.remove(arena.getId());
									GameStates.arenatime.put(arena.getId(), pl.getConfig().getInt("Lobby.Timer"));
								}

							}
				}

			}
		},20L,20L);
		ids.put(arena, id);
	}
	
	public void cancel(Arena a){
		Bukkit.getScheduler().cancelTask(ids.get(a));
	}

	public void startGameTimer(final Arena arena){
		GameStates.arenatime.remove(arena.getId());
		GameStates.arenatime.put(arena.getId(), pl.getConfig().getInt("Game.Time"));
		GameStates.arenajoin.remove(arena.getId());
		GameStates.arenajoin.put(arena.getId(), false);

		int idz = Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, new Runnable() {
			public void run(){

				int timer = GameStates.arenatime.get(arena.getId());
				if(timer >= 0){
				GameStates.arenatime.remove(arena.getId());
				GameStates.arenatime.put(arena.getId(), timer-1);
				if(timer % 60 == 0 && timer != 0){
					broadcastTime("Game", arena, timer);
				}else
					if(timer == 0){
						// End the game
						cancel(arena);
						if(pl.am.getRedKills(arena) > pl.am.getBlueKills(arena)){
							
							pl.end.endGame(arena, "�cRed");
							
						}else
						if(pl.am.getRedKills(arena) < pl.am.getBlueKills(arena)){
							
							pl.end.endGame(arena, "�bBlue");
							
						}else{
							pl.end.endGame(arena, "�9Tie");
						}
						
					}
				}

			}
		},20L,20L);
		
		ids.remove(arena);
		ids.put(arena, idz);
	}

	public void broadcastTime(String t, Arena a, Integer time){
		for(String s : a.getPlayers()){
			Bukkit.getPlayer(s).sendMessage(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString(t+".Broadcast").replace("%TIME%", time+"")));
		}

	}

}
