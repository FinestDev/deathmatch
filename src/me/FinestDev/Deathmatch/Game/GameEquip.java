package me.FinestDev.Deathmatch.Game;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class GameEquip {
	
	public static ItemStack rhelmet = new ItemStack(Material.LEATHER_HELMET, 1);
	public ItemStack rchest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
	public ItemStack rlegg = new ItemStack(Material.LEATHER_LEGGINGS, 1);
	public ItemStack rboots = new ItemStack(Material.LEATHER_BOOTS, 1);
	
	public static ItemStack bhelmet = new ItemStack(Material.LEATHER_HELMET, 1);
	public ItemStack bchest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
	public ItemStack blegg = new ItemStack(Material.LEATHER_LEGGINGS, 1);
	public ItemStack bboots = new ItemStack(Material.LEATHER_BOOTS, 1);
	
	public static ItemStack binoculars = new ItemStack(Material.STICK, 1);
	public static ItemStack switchItem = new ItemStack(Material.LEVER, 1);
	
	public void registerItems(){
		rhelmet = setColor(rhelmet, 255, 0, 0);
		rchest = setColor(rchest, 255, 0, 0);
		rlegg = setColor(rlegg, 255, 0, 0);
		rboots = setColor(rboots, 255, 0, 0);
		
		bhelmet = setColor(bhelmet, 0, 0, 255);
		bchest = setColor(bchest, 0, 0, 255);
		blegg = setColor(blegg, 0, 0, 255);
		bboots = setColor(bboots, 0, 0, 255);
		
		ItemMeta meta = binoculars.getItemMeta();
		meta.setDisplayName("�cBinoculars");
		binoculars.setItemMeta(meta);
		
		meta = switchItem.getItemMeta();
		meta.setDisplayName("�cDetonator");
		switchItem.setItemMeta(meta);
		
	}
	
	public void redEquip(Player p){
		
		p.getInventory().setHelmet(rhelmet);
		p.getInventory().setChestplate(rchest);
		p.getInventory().setLeggings(rlegg);
		p.getInventory().setBoots(rboots);
		
	}
	
	
	public void blueEquip(Player p){
		
		p.getInventory().setHelmet(bhelmet);
		p.getInventory().setChestplate(bchest);
		p.getInventory().setLeggings(blegg);
		p.getInventory().setBoots(bboots);
		
	}
	
	public void equip(Player p){
		
		p.getInventory().addItem(new ItemStack(Material.DIAMOND_SWORD, 1));
		p.getInventory().addItem(new ItemStack(Material.BOW, 1));
		p.getInventory().addItem(new ItemStack(Material.ARROW, 64));
		
		p.getInventory().addItem(binoculars);
		
		p.getInventory().addItem(new ItemStack(Material.REDSTONE, 1));
		
		
	}
	
	public ItemStack setColor(ItemStack i, Integer a, Integer b, Integer c){
		
		LeatherArmorMeta meta = (LeatherArmorMeta) i.getItemMeta();
		meta.setColor(Color.fromRGB(a, b, c));
		i.setItemMeta(meta);
		return i;
		
	}
	
	

}
