package me.FinestDev.Deathmatch.Game;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import me.FinestDev.Deathmatch.Main;
import me.FinestDev.Deathmatch.Arenas.Arena;
import me.FinestDev.Deathmatch.Arenas.GameStates;

public class GameScore {

	Main pl;
	public GameScore(Main in){
		pl = in;
	}
	
	public void removeScoreBoard(Player p){
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard blank = manager.getNewScoreboard();
		p.setScoreboard(blank);
	}

	public void createBoard(final Arena a){
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, new Runnable() {

			public void run(){
				
				Scoreboard board;

				Objective objective;
				ScoreboardManager manager;

				manager = Bukkit.getScoreboardManager();
				board = manager.getNewScoreboard();

				objective = board.registerNewObjective("deathmatch", "dummy");
				objective.setDisplaySlot(DisplaySlot.SIDEBAR);

				objective.setDisplayName("�9Deathmatch");

				Score timer;
				Score timerScore;
				
				Score redKills;
				Score blueKills;
				
				Score redScore;
				Score blueScore;
				
				Score winner;
				Score winScore;
				
				objective.getScore("    ").setScore(12);
				timer = objective.getScore("�aTime Left");
				timer.setScore(11);
				timerScore = objective.getScore("�r"+Math.round( (GameStates.arenatime.get(a.getId())/60.00) * 10.0) / 10.0+"m");
				timerScore.setScore(10);
				
				objective.getScore("   ").setScore(9);
				redKills = objective.getScore("�cRed Kills");
				redKills.setScore(8);

				redScore = objective.getScore("�f"+pl.am.getRedKills(a)+"");
				redScore.setScore(7);

				objective.getScore("  ").setScore(6);

				blueKills = objective.getScore("�bBlue Kills");
				blueKills.setScore(5);

				blueScore = objective.getScore(pl.am.getBlueKills(a)+"");
				blueScore.setScore(4);
				
				objective.getScore(" ").setScore(3);
				
				winner = objective.getScore("�aWinning");
				winner.setScore(2);
				
				if(GameStates.arenajoin.get(a.getId()) == true || pl.am.getRedKills(a) == pl.am.getBlueKills(a)){
					winScore = objective.getScore("�fNone");
				}else
				if(pl.am.getRedKills(a) >= pl.am.getBlueKills(a)){
					winScore = objective.getScore("�cRed");
				}else{
					winScore = objective.getScore("�bBlue");
				}
				winScore.setScore(1);
				
				

				for(String s : a.getPlayers()){
					Player p = Bukkit.getPlayer(s);
					p.setScoreboard(board);
				}

			}

		},20L,20L);


	}

}
