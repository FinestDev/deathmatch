package me.FinestDev.Deathmatch.Game;

import me.FinestDev.Deathmatch.Main;
import me.FinestDev.Deathmatch.Arenas.Arena;
import net.md_5.bungee.api.ChatColor;

public class GameEnd {

	Main pl;
	public GameEnd(Main in){
		pl = in;
	}

	public void endGame(Arena a, String winner){
		
		if(winner != "�9Tie"){
			pl.am.ArenaMessage(a, ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Game.EndMessage").replace("%WINNER%", winner)));
		}else{
			pl.am.ArenaMessage(a, ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Game.TieMessage").replace("%WINNER%", winner)));
		}

		pl.am.resetArena(a);
	}

}
