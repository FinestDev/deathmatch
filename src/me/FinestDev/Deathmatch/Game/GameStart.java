package me.FinestDev.Deathmatch.Game;

import me.FinestDev.Deathmatch.Main;
import me.FinestDev.Deathmatch.Arenas.Arena;
import me.FinestDev.Deathmatch.Weapons.KillStreaks;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class GameStart {
	
	Main pl;
	public GameStart(Main in){
		pl = in;
	}
	
	public void startGame(Arena arena){
		
		for(String p : arena.getPlayers()){
			int redSize = pl.am.getRedTeam(arena).size();
			int blueSize = pl.am.getBlueTeam(arena).size();
			if(redSize > blueSize){
				// Blue
				pl.am.getBlueTeam(arena).add(p);
				Bukkit.getPlayer(p).sendMessage( ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Game.Blue.Join")) );
				teleport(Bukkit.getPlayer(p), pl.util.fromString(pl.getConfig().getString("Arenas."+arena.getId()+".Blue")));
				pl.equip.blueEquip(Bukkit.getPlayer(p));
			}else{
				// Red
				pl.am.getRedTeam(arena).add(p);
				Bukkit.getPlayer(p).sendMessage( ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Game.Red.Join")) );
				teleport(Bukkit.getPlayer(p), pl.util.fromString(pl.getConfig().getString("Arenas."+arena.getId()+".Red")));
				pl.equip.redEquip(Bukkit.getPlayer(p));
			}
			KillStreaks.kills.put(p, 1);
			pl.equip.equip(Bukkit.getPlayer(p));
		}
		
	}
	
	public void teleport(Player p, Location l){
		p.teleport(l);
	}

}
