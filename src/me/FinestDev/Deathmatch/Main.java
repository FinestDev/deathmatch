package me.FinestDev.Deathmatch;

import me.FinestDev.Deathmatch.Arenas.ArenaManager;
import me.FinestDev.Deathmatch.Arenas.ArenaTimers;
import me.FinestDev.Deathmatch.Arenas.GameStates;
import me.FinestDev.Deathmatch.Events.Death;
import me.FinestDev.Deathmatch.Events.Explode;
import me.FinestDev.Deathmatch.Events.Items;
import me.FinestDev.Deathmatch.Events.Quit;
import me.FinestDev.Deathmatch.Events.Signs;
import me.FinestDev.Deathmatch.Events.Teams;
import me.FinestDev.Deathmatch.Game.GameEnd;
import me.FinestDev.Deathmatch.Game.GameEquip;
import me.FinestDev.Deathmatch.Game.GameScore;
import me.FinestDev.Deathmatch.Game.GameStart;
import me.FinestDev.Deathmatch.Utilities.Utilities;
import me.FinestDev.Deathmatch.Weapons.Binoculars;
import me.FinestDev.Deathmatch.Weapons.KillStreaks;
import me.FinestDev.Deathmatch.Weapons.Vest;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	public ArenaManager am = new ArenaManager(this);
	public ArenaTimers timers = new ArenaTimers(this);
	public GameEquip equip = new GameEquip();
	public Utilities util = new Utilities(this);
	public GameStart start = new GameStart(this);
	
	public GameEnd end = new GameEnd(this);
	public GameScore score = new GameScore(this);

	public void onEnable(){
		saveDefaultConfig();

		am.loadGames();
		equip.registerItems();

		getCommand("deathmatch").setExecutor(new Commands(this));

		Bukkit.getPluginManager().registerEvents(new Death(this), this);
		Bukkit.getPluginManager().registerEvents(new Quit(this), this);
		Bukkit.getPluginManager().registerEvents(new Teams(this), this);
		Bukkit.getPluginManager().registerEvents(new Explode(this), this);
		Bukkit.getPluginManager().registerEvents(new Binoculars(this), this);
		Bukkit.getPluginManager().registerEvents(new KillStreaks(this), this);
		Bukkit.getPluginManager().registerEvents(new Items(this), this);
		Bukkit.getPluginManager().registerEvents(new Vest(this), this);
		Bukkit.getPluginManager().registerEvents(new Signs(this), this);

	}

	public void onDisable(){
		for(Player p : Bukkit.getOnlinePlayers()){
			am.removePlayer(p);
		}
		
		saveConfig();

		GameStates.arenajoin.clear();
		GameStates.arenatime.clear();
	}
}
