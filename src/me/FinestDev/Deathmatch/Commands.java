package me.FinestDev.Deathmatch;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor {

	Main pl;
	public Commands(Main in){
		pl = in;
	}

	private static HashMap<String, Location> spawn = new HashMap<String, Location>();
	private static HashMap<String, Location> spawnloc = new HashMap<String, Location>();

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(sender instanceof Player){
			Player p = (Player) sender;

			if(cmd.getName().equalsIgnoreCase("deathmatch")){
				if(args.length >= 1){

					if(args[0].equals("join")){
						if(args.length == 2){
							pl.am.addPlayer(p, Integer.parseInt(args[1]));	
						}else{
							p.sendMessage("Please specify an arena!");
						}
					}else
						if(args[0].equals("leave")){
							pl.am.removePlayer(p);
						}else
							if(args[0].equals("create")){
								if(p.hasPermission("deathmatch.create")){
									if(args.length == 2){

										if(args[1].equals("lobby")){
											if(!spawn.containsKey(p.getName())){
												spawn.put(p.getName(), p.getLocation());
												p.sendMessage("You have selected the lobby spawn point! Please choose the RED spawn (/deathmatch create red)! Use /deathmatch cancel To cancel this process!");
											}
										}else
										if(args[1].equals("red")){
											if(!spawnloc.containsKey(p.getName())){

												p.sendMessage("You've set the RED spawn! Please choose the BLUE spawn now (/deathmatch create blue)!");
												spawnloc.put(p.getName(), p.getLocation());

											}
										}else
										if(args[1].equals("blue")){

											if(spawn.containsKey(p.getName()) && spawnloc.containsKey(p.getName())){
												p.sendMessage("You've set the BLUE spawn! Arena successfully created!");
												pl.am.createArena(spawn.get(p.getName()), spawnloc.get(p.getName()), p.getLocation());
												spawn.remove(p.getName());
												spawnloc.remove(p.getName());

											}else{
												p.sendMessage("Make sure you've selected a LOBBY, RED, and BLUE spawn point!");
											}
										}
									}
								}
							}else
								if(args[0].equals("cancel")){
									if(spawn.containsKey(p.getName())){
										spawn.remove(p.getName());
										p.sendMessage("Arena creation canceled");
									}
									if(spawnloc.containsKey(p.getName())){
										spawnloc.remove(p.getName());
									}
								}
				}

			}

		}
		return false;	
	}

}
