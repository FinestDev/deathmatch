package me.FinestDev.Deathmatch.Events;

import me.FinestDev.Deathmatch.Main;
import me.FinestDev.Deathmatch.Weapons.KillStreaks;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

public class Death implements Listener {

	Main pl;
	public Death(Main in){
		pl = in;
	}

	@EventHandler
	public void onDrop(PlayerDeathEvent e){

		if(pl.am.isInGame(e.getEntity())){
			
			pl.am.checkGame(pl.am.getPlayer(e.getEntity()));
			
			if(pl.util.getTeam(e.getEntity()).equals("Red")){
				pl.am.setBlueKills(pl.am.getPlayer(e.getEntity()));
			}else{
				pl.am.setRedKills(pl.am.getPlayer(e.getEntity()));
			}
			e.getDrops().clear();
		}
		
	}

	@EventHandler
	public void onDeath(PlayerRespawnEvent e){
		final Player p = e.getPlayer();

		if(pl.am.isInGame(p)){

			KillStreaks.kills.remove(p.getName());
			KillStreaks.kills.put(p.getName(), 1);

			Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable(){
				public void run(){

					switch(pl.util.getTeam(p)){
					case "Blue":
						pl.start.teleport(p, pl.util.fromString(pl.getConfig().getString("Arenas."+pl.am.getPlayer(p).getId()+".Blue")));
						pl.equip.blueEquip(p);
						break;
					case "Red":
						pl.start.teleport(p, pl.util.fromString(pl.getConfig().getString("Arenas."+pl.am.getPlayer(p).getId()+".Red")));
						pl.equip.redEquip(p);				
						break;
					}

					pl.equip.equip(p);
					p.getInventory().remove(Material.REDSTONE);
					p.getInventory().addItem(new ItemStack(Material.REDSTONE, KillStreaks.kills.get(p.getName())));

				}
			},10L);

		}
	}

}
