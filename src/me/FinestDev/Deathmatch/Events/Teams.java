package me.FinestDev.Deathmatch.Events;

import me.FinestDev.Deathmatch.Main;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class Teams implements Listener {

	Main pl;
	public Teams(Main in){
		pl = in;
	}

	@EventHandler
	public void onTeamHit(EntityDamageByEntityEvent e){
		if(e.getEntity() instanceof Player){
			Player hit = (Player) e.getEntity();

			if(e.getDamager() instanceof Arrow){
				Arrow arrow = (Arrow) e.getDamager();

				if(arrow.getShooter() instanceof Player){
					Player shooter = (Player) arrow.getShooter();

					if(pl.am.isInGame(hit) && pl.am.isInGame(shooter)){
						String hitTeam = pl.util.getTeam(hit);
						String dmgTeam = pl.util.getTeam(shooter);

						if(hitTeam == null || dmgTeam == null){
							return;
						}
						
						if(hitTeam.equals(dmgTeam)){
							e.setCancelled(true);
						}
					}


				}
			}
			if(e.getDamager() instanceof Player){

				Player damager = (Player) e.getDamager();

				if(pl.am.isInGame(hit) && pl.am.isInGame(damager)){
					String hitTeam = pl.util.getTeam(hit);
					String dmgTeam = pl.util.getTeam(damager);
					
					if(hitTeam == null || dmgTeam == null){
						return;
					}

					if(hitTeam.equals(dmgTeam)){
						e.setCancelled(true);					
					}
				}
			}
		}
	}

}
