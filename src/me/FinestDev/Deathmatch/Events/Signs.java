package me.FinestDev.Deathmatch.Events;

import me.FinestDev.Deathmatch.Main;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class Signs implements Listener {

	Main pl;
	public Signs(Main in){
		pl = in;
	}

	@EventHandler
	public void onCreate(SignChangeEvent e){

		if(e.getLine(0).equals("[DeathMatch]")){

			if(e.getLine(1).equals("Join")){

				if( pl.am.getArena( Integer.parseInt(e.getLine(2))) != null ){

					if(e.getPlayer().hasPermission("deathmatch.signs")){

						e.setLine(0, ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Signs.First")) );
						e.setLine(1, ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Signs.Second")) );
						e.setLine(2, ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Signs.Third").replace("%ARENA%", e.getLine(2))) );

					}	
				}	
			}
		}
	}

	@EventHandler
	public void onClick(PlayerInteractEvent e){
		if(e.getClickedBlock() != null){

			if(e.getClickedBlock().getType() == Material.SIGN || e.getClickedBlock().getType() == Material.WALL_SIGN || e.getClickedBlock().getType() == Material.SIGN_POST){
				if (e.getClickedBlock().getState() instanceof Sign) {

					Sign sign = (Sign) e.getClickedBlock().getState();
					
					
					if(sign.getLine(0).equals( ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Signs.First")) )){
							
						if(sign.getLine(1).equals( ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Signs.Second")) )){
							
							pl.am.addPlayer(e.getPlayer(), Integer.parseInt( ChatColor.stripColor(sign.getLine(2))));
							
							
						}
						
						
					}

				}



			}
		}
	}

}
