package me.FinestDev.Deathmatch.Events;

import me.FinestDev.Deathmatch.Main;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerDropItemEvent;

public class Items implements Listener {

	Main pl;
	public Items(Main in){
		pl = in;
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent event){
		Player p = event.getPlayer();

		if(pl.am.isInGame(p)){

			event.setCancelled(true);

		}
	}

	@EventHandler
	public void onArmor(InventoryClickEvent event){
		Player p = (Player) event.getWhoClicked();

		if(pl.am.isInGame(p)){
			if(event.getSlotType() == SlotType.ARMOR){
				event.setCancelled(true); // Cancel it
			}
		}
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent event){
		
		if(pl.am.isInGame(event.getPlayer())){
			event.setCancelled(true);
		}
		
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent event){
		
		if(pl.am.isInGame(event.getPlayer())){
			event.setCancelled(true);
		}
		
	}

}
