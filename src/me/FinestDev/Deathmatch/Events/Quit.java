package me.FinestDev.Deathmatch.Events;

import me.FinestDev.Deathmatch.Main;
import me.FinestDev.Deathmatch.Weapons.KillStreaks;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Quit implements Listener {

	Main pl;
	public Quit(Main in){
		pl = in;
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		
		if(pl.am.isInGame(p)){
			pl.am.removePlayer(p);
		}
		
		if(KillStreaks.kills.containsKey(p.getName())){
			KillStreaks.kills.remove(p.getName());
		}
		
	}

}