package me.FinestDev.Deathmatch.Events;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import me.FinestDev.Deathmatch.Main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;

public class Explode implements Listener {

	Main pl;

	public Explode(Main in) {
		pl = in;
	}

	public static HashMap<Location, String> map = new HashMap<Location, String>();
	public static HashMap<Entity, Location> map2 = new HashMap<Entity, Location>();
	
	public void teleport(){
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, new Runnable(){
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public void run(){
				Iterator it = map2.entrySet().iterator();
				while (it.hasNext()) {

					Map.Entry<Entity, Location> pair = (Map.Entry<Entity, Location>) it
							.next();
					
					if(pair.getKey().isValid()){
						pair.getKey().teleport(pair.getValue());
					}

					

					it.remove(); // avoids a ConcurrentModificationException
				}
			}
		},3L,3L);
		
	}

	@EventHandler
	public void onExplode(EntityDamageByEntityEvent e) {

		if (e.getDamager() instanceof TNTPrimed) {
			
			if(e.getEntity() instanceof TNTPrimed){
				e.setCancelled(true);
			}

			if (e.getEntity() instanceof Player) {

				Player p = (Player) e.getEntity();
				Entity en = e.getDamager();

				String shooter = checkDistance(en.getLocation());

				if (shooter != null) {
					String team = pl.util.getTeam(Bukkit.getPlayer(shooter));
					
					if(pl.util.getTeam(p).equals(team)){
						e.setCancelled(true);
					}else{
						e.setDamage(e.getDamage()+15.0);
					}

				}

			}

		}

	}
	
	@EventHandler
	public void onPrime(ExplosionPrimeEvent e){
		
		if(e.getEntity() instanceof TNTPrimed){
			TNTPrimed p = (TNTPrimed) e.getEntity();
			
			p.setTicksLived(15);
		}
		
	}
	
	@EventHandler
	public void onTNT(final EntityExplodeEvent e){
		
		if(e.getEntity() instanceof TNTPrimed){
			e.blockList().clear();
			if(checkDistance(e.getEntity().getLocation()) != null){
				map2.remove(e.getEntity().getLocation());
				Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
					public void run(){
						
						map.remove(e.getEntity().getLocation());
						
					}
				},20L);
			}
		}
		
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String checkDistance(Location l) {

		Iterator it = map.entrySet().iterator();
		while (it.hasNext()) {

			Map.Entry<Location, String> pair = (Map.Entry<Location, String>) it
					.next();

			if (l.distance((Location) pair.getKey()) <= 3) {
				return pair.getValue();
			}

			it.remove(); // avoids a ConcurrentModificationException
		}

		return null;
	}

}
