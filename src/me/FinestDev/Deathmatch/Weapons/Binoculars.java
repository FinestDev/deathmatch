package me.FinestDev.Deathmatch.Weapons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import me.FinestDev.Deathmatch.Main;
import me.FinestDev.Deathmatch.Events.Explode;
import me.FinestDev.Deathmatch.Game.GameEquip;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Binoculars implements Listener {

	Main pl;

	public Binoculars(Main in) {
		pl = in;
	}

	public static List<String> cd = new ArrayList<String>();
	public static HashMap<Player, Location> tnt	= new HashMap<Player, Location>(); 

	@EventHandler
	public void onUse(PlayerInteractEvent e) {
		final Player p = e.getPlayer();

		if (p.getItemInHand().equals(GameEquip.binoculars)) {
			// Zoom
			if (e.getAction().name().contains("RIGHT")) {

				if (p.hasPotionEffect(PotionEffectType.SLOW)) {
					p.removePotionEffect(PotionEffectType.SLOW);
				} else {
					p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,
							Integer.MAX_VALUE, 10));
				}

			} else if (e.getAction().name().contains("LEFT")) {

				if (cd.contains(p.getName())) {
					p.sendMessage( ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Binoculars.CDMessage")) );
					return;
				}

				Block target = e.getPlayer().getTargetBlock(
						(Set<Material>) null, pl.getConfig().getInt("Binoculars.Range"));

				if (target.getType() != Material.AIR
						&& target.getType() != Material.WATER
						&& target.getType() != Material.LAVA) {

					if(!p.getInventory().contains(Material.TNT)){ p.sendMessage("�cYou don't have any TNT!"); return; }

					@SuppressWarnings("deprecation")
					int amount = getAmount(p, Material.TNT.getId());
					p.getInventory().remove(Material.TNT);

					Entity ent = null;
					for(int i = 0; i < amount; i ++){
						ent = p.getWorld().spawnEntity(target.getLocation().add(i, 8, 0),
							EntityType.PRIMED_TNT);
						Explode.map.put(target.getLocation().add(i,0,0), p.getName());
					}

					cd.add(p.getName());
					Bukkit.getScheduler().scheduleSyncDelayedTask(pl,
							new Runnable() {
						public void run() {
							cd.remove(p.getName());
						}
					},
					pl.getConfig().getInt("Binoculars.Cooldown") * 20L);
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	public static int getAmount(Player player, int id)
	{
		PlayerInventory inventory = player.getInventory();
		ItemStack[] items = inventory.getContents();
		int has = 0;
		for (ItemStack item : items)
		{
			if ((item != null) && (item.getTypeId() == id) && (item.getAmount() > 0))
			{
				has += item.getAmount();
			}
		}
		return has;
	}

}
