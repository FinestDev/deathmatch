package me.FinestDev.Deathmatch.Weapons;

import java.util.HashMap;

import me.FinestDev.Deathmatch.Main;
import me.FinestDev.Deathmatch.Game.GameEquip;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

public class KillStreaks implements Listener {

	Main pl;
	public KillStreaks(Main in){
		pl = in;
	}

	public static HashMap<String, Integer> kills = new HashMap<String, Integer>();
	
	@EventHandler
	public void onPlace(BlockPlaceEvent e){
		if(e.getBlock().getType() == Material.TNT){
			
			Player p = e.getPlayer();
			
			if(pl.am.isInGame(p)){
				
				e.setCancelled(true);
				
			}
			
		}
	}

	@EventHandler
	public void onKillStreak(PlayerDeathEvent e) {

		if (e.getEntity().getKiller() instanceof Player) {
			Player killer = (Player) e.getEntity().getKiller();

			if(pl.am.isInGame(killer)){
				String name = killer.getName();
				int killcount = kills.get(name);

				kills.remove(name);
				kills.put(name, killcount);

				switch(killcount){
				case 5:
					killer.getInventory().addItem(new ItemStack(Material.TNT, 3));
					break;
				case 10:
					killer.getInventory().addItem(new ItemStack(Material.TNT, 6));
					break;
				case 15:
					killer.getInventory().addItem(new ItemStack(Material.TNT, 9));
					break;
				case 20:
					killer.getInventory().addItem(GameEquip.switchItem);
					killer.getInventory().setHelmet(new ItemStack(Material.TNT, 1));
					break;
				}
				killer.getInventory().remove(Material.REDSTONE);
				killer.getInventory().addItem(new ItemStack(Material.REDSTONE, killcount));
			}
		}

	}

}
