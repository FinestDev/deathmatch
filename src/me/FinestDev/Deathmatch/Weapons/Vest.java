package me.FinestDev.Deathmatch.Weapons;


import me.FinestDev.Deathmatch.Main;
import me.FinestDev.Deathmatch.Events.Explode;
import me.FinestDev.Deathmatch.Game.GameEquip;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class Vest implements Listener {

	Main pl;
	public Vest(Main in) {
		pl = in;
	}

	@EventHandler
	public void onActivate(PlayerInteractEvent e){
		Player p = e.getPlayer();

		if(e.getAction().name().contains("RIGHT")){
			if(pl.am.isInGame(p)){

				if(p.getItemInHand() != null){
					if(p.getItemInHand().getItemMeta() != null){
						if(p.getItemInHand().getItemMeta().getDisplayName() != null){
							if(p.getItemInHand().getItemMeta().getDisplayName().equals(GameEquip.switchItem.getItemMeta().getDisplayName())){
								if(p.getInventory().getHelmet().getType() == Material.TNT){
									e.setCancelled(true);
									p.setItemInHand(null);
									if(pl.util.getTeam(p).equals("Red")){
										p.getInventory().setHelmet(GameEquip.rhelmet);
									}else{
										p.getInventory().setHelmet(GameEquip.bhelmet);
									}
									Location loc = p.getLocation();
									loc.getWorld().createExplosion(loc, 4.0F, false);
									
									for(Entity e1 : p.getNearbyEntities(5, 5, 5)){
										if(e1 instanceof Player){
											((Player) e1).damage(20.0F);
										}
									}
									
									Explode.map.put(p.getLocation(), p.getName());
								}
							}
						}
					}
				}


			}


		}

	}

}
