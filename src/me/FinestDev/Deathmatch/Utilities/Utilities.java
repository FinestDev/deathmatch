package me.FinestDev.Deathmatch.Utilities;

import me.FinestDev.Deathmatch.Main;
import me.FinestDev.Deathmatch.Arenas.Arena;
import me.FinestDev.Deathmatch.Arenas.ArenaManager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Utilities {
	
	Main pl;
	public Utilities(Main in){
		pl = in;
	}
	
	public Location fromString(String s){
		String[] spl = s.split(",");

		return new Location(Bukkit.getWorld(spl[0]),Double.parseDouble(spl[1]),Double.parseDouble(spl[2]),Double.parseDouble(spl[3]));
	}
	
	public String getTeam(Player p){
		for(Arena a : ArenaManager.arenas){
			if(a.getPlayers().contains(p.getName())){
				if(pl.am.getRedTeam(a).contains(p.getName())){
					return "Red";
				}else{
					return "Blue";
				}
			}
		}
		return "None";
	}

}
